---
note_type: paper_reading
title: works in <strong>canonical spaces</strong>
title_cn: 标准正视图空间的有关研究
not_published: true
---

---

## NOCS系列

**[[NOCS]](https://geometry.stanford.edu/projects/NOCS_CVPR2019/)** NOCS for Pose Estimation [CVPR2019]

**[[X-NOCS]](https://geometry.stanford.edu/projects/xnocs/)** Two-intersection NOCS for shape reconstruction [NeurIPS2019]

**[[ANCSH]](https://articulated-pose.github.io/)** Articulated Pose Estimation [CVPR2020]

**[[S-NOCS]](https://geometry.stanford.edu/projects/pix2surf/)** Shape reconstruction in NOCS with Surfaces (This work) [ECCV2020]

**[[T-NOCS]](https://geometry.stanford.edu/projects/caspr/)** NOCS along Time Axis [arXiv2020 Pre-print]

## learning a canonical representation from non-canonical data

- GIRAFFE
- spectralGAN